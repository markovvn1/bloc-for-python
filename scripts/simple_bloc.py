import asyncio
from dataclasses import dataclass

from PyQt6 import QtWidgets

from bloc import Bloc, Emitter, PyQt6Worker


@dataclass(frozen=True)
class SplashLogoEvent:
    pass


@dataclass(frozen=True)
class InitialEvent(SplashLogoEvent):
    pass


@dataclass(frozen=True)
class SplashLogoState:
    pass


@dataclass(frozen=True)
class InitialState(SplashLogoState):
    pass


@dataclass(frozen=True)
class LoadingProgressState(SplashLogoState):
    progress: int = 0


@dataclass(frozen=True)
class LoadingCompleteState(SplashLogoState):
    pass


class LoadingBloc(Bloc[SplashLogoEvent, SplashLogoState]):
    def __init__(self) -> None:
        super().__init__(InitialState())
        self.on(self.on_initial_event)

    async def on_initial_event(self, _event: InitialEvent, emit: Emitter[SplashLogoState]) -> None:
        for i in range(0, 101, 20):
            emit(LoadingProgressState(i))
            await asyncio.sleep(1)
        emit(LoadingCompleteState())


if __name__ == "__main__":
    app = QtWidgets.QApplication([])

    bloc_worker = PyQt6Worker()
    bloc_worker.start()

    def listener(_old_state: SplashLogoState, new_state: SplashLogoState) -> None:
        print(new_state)
        if isinstance(new_state, LoadingCompleteState):
            app.exit()

    bloc = LoadingBloc()
    bloc.add(InitialEvent())
    bloc.register_listener(listener)

    app.exec()

    bloc_worker.stop()
    bloc_worker.join()
