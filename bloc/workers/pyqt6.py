import asyncio
import threading
from typing import Callable, Coroutine, Set

from loguru import logger
from PyQt6 import QtCore

from ..core import BlocBaseWorker


class _PyQt6WorkerThread(threading.Thread):

    _loop: asyncio.AbstractEventLoop
    _bg_tasks: Set["asyncio.Task[None]"]
    _stopping: bool = False

    def __init__(self) -> None:
        super().__init__()
        self._bg_tasks = set()
        self._loop = asyncio.new_event_loop()

    def run(self) -> None:
        asyncio.set_event_loop(self._loop)
        logger.info("Starting BLoC worker in a thread")
        self._loop.run_forever()

    def __check_stop(self) -> None:
        if not self._stopping:
            return
        if len(self._bg_tasks) > 0:
            logger.debug("Cannot stop BLoC worker: waiting for {} tasks", len(self._bg_tasks))
            return
        self._stopping = False
        logger.info("Stopping BLoC worker: all tasks were complete")
        self._loop.stop()

    def stop(self) -> None:
        self._stopping = True
        self._loop.call_soon_threadsafe(self.__check_stop)

    def add(self, task: Callable[[], Coroutine[None, None, None]]) -> None:
        def wrapper() -> None:
            res: "asyncio.Task[None]" = asyncio.create_task(task())
            self._bg_tasks.add(res)
            res.add_done_callback(self._bg_tasks.discard)
            res.add_done_callback(lambda _: self.__check_stop())

        self._loop.call_soon_threadsafe(wrapper)


class PyQt6Worker(BlocBaseWorker, QtCore.QObject):
    state_queue = QtCore.pyqtSignal(object)

    _thread: _PyQt6WorkerThread
    _emitted_tasks: int = 0
    _completed_tasks: int = 0
    _next_warn_level: int = 1000

    def __init__(self) -> None:
        super().__init__()
        self._thread = _PyQt6WorkerThread()
        self.state_queue.connect(self._process_state)

    def start(self) -> None:
        self._thread.start()

    def stop(self) -> None:
        self._thread.stop()

    def join(self) -> None:
        self._thread.join()

    @QtCore.pyqtSlot(object)
    def _process_state(self, task: object) -> None:
        self._completed_tasks += 1
        assert callable(task)
        task()

    def _on_add(self, task: Callable[[], Coroutine[None, None, None]]) -> None:
        self._thread.add(task)

    def _on_emit(self, task: Callable[[], None]) -> None:
        self.state_queue.emit(task)
        self._emitted_tasks += 1
        if self._emitted_tasks - self._completed_tasks > self._next_warn_level:
            logger.warning("More than {} tasks waiting to be processed", self._next_warn_level)
            self._next_warn_level += 1000
