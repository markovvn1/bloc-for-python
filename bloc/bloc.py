import inspect
from typing import Any, Awaitable, Callable, Dict, Generic, Optional, Tuple, Type, TypeVar

from loguru import logger

from .core import BlocCore
from .emitter import Emitter

EventBase = TypeVar("EventBase")
StateBase = TypeVar("StateBase")


class Bloc(Generic[EventBase, StateBase]):
    __initialized: bool = False
    __base_event_type: Type[EventBase]
    __base_state_type: Type[StateBase]
    __callbacks: Dict[Type[EventBase], Callable[[Any, Emitter[StateBase]], Optional[Awaitable[None]]]]
    __core: BlocCore
    __emitter: Emitter[StateBase]
    __state: StateBase
    __listeners: Tuple[Callable[[StateBase, StateBase], None], ...]

    @property
    def state(self) -> StateBase:
        """Current state."""
        return self.__state

    def __init__(
        self, initial_state: StateBase, listener: Optional[Callable[[StateBase, StateBase], None]] = None
    ) -> None:
        super().__init__()
        orig = self.__orig_bases__  # type: ignore # pylint: disable=no-member
        assert len(orig) == 1
        assert len(orig[0].__args__) == 2
        self.__base_event_type, self.__base_state_type = orig[0].__args__
        self.__callbacks = {}
        self.__core = BlocCore.get_instance()
        self.__emitter = Emitter(self.__emit)
        self._assert_state_type(initial_state)
        self.__state = initial_state
        self.__listeners = ()
        self.__initialized = True

        if listener:
            self.register_listener(listener)
            self.__emit(initial_state)

    def _assert_state_type(self, state: StateBase) -> None:
        if not isinstance(state, self.__base_state_type):
            raise ValueError(
                f"Invalid state type. Got {state.__class__.__name__}, but "
                f"expected subclass of {self.__base_state_type.__name__}"
            )

    def _assert_event_type(self, event: EventBase) -> None:
        if not isinstance(event, self.__base_event_type):
            raise ValueError(
                f"Invalid event type. Got {event.__class__.__name__}, but "
                f"expected subclass of {self.__base_event_type.__name__}"
            )

    def __emit(self, state: StateBase) -> None:
        self._assert_state_type(state)

        old_state = self.__state
        self.__state = state

        def wrap() -> None:
            for listener in self.__listeners:
                listener(old_state, state)

        self.__core.emit(wrap)

    def add(self, event: EventBase) -> None:
        if not self.__initialized:
            raise RuntimeError("Bloc has not been initialized. Call __init__ first.")
        if not isinstance(event, self.__base_event_type):
            raise ValueError(
                f"Invalid event type. Got {event.__class__.__name__}, but "
                f"expected subclass of {self.__base_event_type.__name__}"
            )

        if event.__class__ not in self.__callbacks:
            raise ValueError(f"No callbacks were registred for event type {event.__class__.__name__}")

        @logger.catch
        async def wrap() -> None:
            callback = self.__callbacks[event.__class__]
            emitter = self.__emitter
            res = callback(event, emitter)
            if inspect.iscoroutine(res):
                await res

        self.__core.add(wrap)

    def on(self, callback: Callable[[Any, Emitter[StateBase]], Optional[Awaitable[None]]]) -> None:
        if not self.__initialized:
            raise RuntimeError("Bloc has not been initialized. Call __init__ first.")

        sig = inspect.signature(callback)
        if len(sig.parameters) != 2:
            raise ValueError(
                f"Invalid signature for callback. Callback accept {len(sig.parameters)} parameters, but expected 2"
            )

        params = list(sig.parameters.items())
        event_class, emitter_class = params[0][1].annotation, params[1][1].annotation
        if not issubclass(event_class, self.__base_event_type):
            raise ValueError(
                f"Invalid event type for callback. Callback accept {event_class.__name__}, but "
                f"expected subclass of {self.__base_event_type.__name__}"
            )

        if not hasattr(emitter_class, "__origin__") or emitter_class.__origin__ != Emitter:
            raise ValueError(
                f"Invalid emitter type for callback. Callback accept {emitter_class.__name__}, but "
                f"expected {Emitter.__name__}"
            )

        if event_class in self.__callbacks:
            raise ValueError(f"Callback for the given event ({event_class.__name__}) has already been registered")
        self.__callbacks[event_class] = callback

    def register_listener(self, listener: Callable[[StateBase, StateBase], None]) -> None:
        if not self.__initialized:
            raise RuntimeError("Bloc has not been initialized. Call __init__ first.")

        sig = inspect.signature(listener)
        if len(sig.parameters) != 2:
            raise ValueError(
                f"Invalid signature for listener. Listener accept {len(sig.parameters)} parameters, but expected 2"
            )
        listeners = list(self.__listeners)
        listeners.append(listener)
        self.__listeners = tuple(listeners)

    def unregister_listener(self, listener: Callable[[StateBase, StateBase], None]) -> None:
        if not self.__initialized:
            raise RuntimeError("Bloc has not been initialized. Call __init__ first.")

        if listener not in self.__listeners:
            raise ValueError("The listener was not registered")
        listeners = list(self.__listeners)
        listeners.remove(listener)
        self.__listeners = tuple(listeners)
