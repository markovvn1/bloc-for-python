from typing import Callable, Coroutine, Optional
from weakref import ReferenceType, ref


class BlocCore:
    __instance: Optional["BlocCore"] = None
    _worker: Optional["ReferenceType[BlocBaseWorker]"] = None

    @property
    def worker(self) -> "BlocBaseWorker":
        if self._worker is None:
            raise RuntimeError("No worker has been created")
        if (worker := self._worker()) is None:
            raise RuntimeError("Worker has died")
        return worker

    @classmethod
    def get_instance(cls) -> "BlocCore":
        if cls.__instance is None:
            cls.__instance = cls()
        return cls.__instance

    def set_worker(self, worker: "BlocBaseWorker") -> None:
        if self._worker is not None:
            raise RuntimeError(
                "Only one worker can be created for the Bloc. Worker with "
                f"name {self._worker.__class__.__name__} is already set"
            )
        self._worker = ref(worker)

    def __init__(self) -> None:
        if BlocCore.__instance is not None:
            raise RuntimeError("BlocCore is a singleton!")

    def add(self, task: Callable[[], Coroutine[None, None, None]]) -> None:
        self.worker._on_add(task)

    def emit(self, task: Callable[[], None]) -> None:
        self.worker._on_emit(task)


class BlocBaseWorker:
    def __init__(self) -> None:
        super().__init__()
        BlocCore.get_instance().set_worker(self)

    def _on_add(self, task: Callable[[], Coroutine[None, None, None]]) -> None:
        pass

    def _on_emit(self, task: Callable[[], None]) -> None:
        pass
