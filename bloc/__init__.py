from typing import Any

from .bloc import Bloc
from .emitter import Emitter

__all__ = ["Bloc", "Emitter"]

__version__ = "1.0.1"

try:
    from .workers.pyqt6 import PyQt6Worker

    __all__.append(PyQt6Worker.__name__)
except ImportError:
    pass


def __getattr__(name: str) -> Any:
    if name == "PyQt6Worker":
        from .workers.pyqt6 import PyQt6Worker as _PyQt6Worker

        return _PyQt6Worker
    raise AttributeError(f"module '{__name__}' has no attribute '{name}'")
