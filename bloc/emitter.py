from typing import Callable, Generic, Optional, TypeVar
from weakref import ref

StateBase = TypeVar("StateBase")


class Emitter(Generic[StateBase]):
    __on_emit: Optional[Callable[[StateBase], None]]

    def __init__(self, on_emit: Callable[[StateBase], None]):
        _this, _func = ref(on_emit.__self__), ref(on_emit.__func__)  # type: ignore

        def on_emit_wrapper(state: StateBase) -> None:
            if (this := _this()) is None or (func := _func()) is None:
                raise RuntimeError("Bloc is destroyed")
            func(this, state)

        self.__on_emit = on_emit_wrapper

    def __call__(self, state: StateBase) -> None:
        assert self.__on_emit is not None
        self.__on_emit(state)
