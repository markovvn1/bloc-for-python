VENV ?= .venv
CODE = bloc scripts

.PHONY: venv
venv:
	python -m venv $(VENV)
	$(VENV)/bin/python -m pip install --upgrade pip
	$(VENV)/bin/python -m pip install poetry
	$(VENV)/bin/poetry install -E pyqt6

.PHONY: test
test:
	$(VENV)/bin/pytest -v tests


.PHONY: lint
lint:
	# check package version
	$(VENV)/bin/python -c "import bloc as a; exit(a.__version__ != \"`poetry version -s`\")"
	$(VENV)/bin/poetry lock --check
	# python linters
	$(VENV)/bin/pflake8 --jobs 4 --statistics --show-source $(CODE)
	$(VENV)/bin/pylint --jobs 4 $(CODE)
	$(VENV)/bin/mypy --show-error-codes $(CODE)
	$(VENV)/bin/black --skip-string-normalization --check $(CODE)

.PHONY: format
format:
	$(VENV)/bin/isort $(CODE)
	$(VENV)/bin/black $(CODE)
	$(VENV)/bin/pautoflake --recursive --remove-all-unused-imports --in-place $(CODE)


.PHONY: ci
ci:	lint test
