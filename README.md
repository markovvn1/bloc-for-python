<div align="center">
  <img src=".gitlab/logo.webp" height="124px"/><br/>
  <h1>BLoC for Python</h1>
  <p>Repository with implementation of BLoC architecture for PyQt6</a></p>
</div>


## 📝 About The Project

**BLoC** is a popular design/architectural pattern used in software development to design and develop applications.

This repository contains a python package with BLoC implementation for Python (for PyQt6).

## ⚡️ Quick start

Install package using *pip* package manager:

```bash
pip install --index-url https://pypi:vnSAzGMyxFo2xY7sSjeN@gitlab.com/api/v4/groups/54262453/-/packages/pypi/simple bloc[pyqt6]
```

### :star: Poetry-way to quick start

To install this package with *poetry* you have to add the new source to your `pyproject.toml` file:

```bash
[[tool.poetry.source]]
name = "mirai-libs"
url = "https://gitlab.com/api/v4/groups/54262453/-/packages/pypi/simple"
```

And add credentials for the read-only package repository access to the `poetry.toml` file:

```bash
[repositories]
[repositories.mirai-libs]
url = "https://gitlab.com/api/v4/groups/54262453/-/packages/pypi/simple"

[http-basic]
[http-basic.mirai-libs]
username = "pypi"
password = "vnSAzGMyxFo2xY7sSjeN"
```

You can then add the package as a dependency:

```bash
poetry add --source mirai-libs bloc -E pyqt6
```

### :diamond_shape_with_a_dot_inside: Extras

Extras allow you to choose which workers you want to install. There are currently 1 extension available:

* `pyqt6` - Add PyQt6Worker - BLoC worker for PyQt6

### ⚙️ Developing

Download the repository and change the current directory:

```bash
git clone git@gitlab.com:mirai-vision/libs/bloc.git && cd bloc
```

Configure virtual environment. Make sure a `.venv` folder has been created after this step.

```bash
make venv
# source .venv/bin/activate
```

Goto `scripts` folder and run some examples.

Run linters, formaters and tests:
```bash
make lint  # check code quality
make format  # beautify code
make test  # run tests
```

## :computer: Contributors

<p>

  :boy: <b>Vladimir Markov</b> <br>
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Email: <a>Markovvn1@gmail.com</a> <br>
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; GitLab: <a href="https://gitlab.com/markovvn1">@markovvn1</a> <br>
</p>